package com.ultra

object Config {

  val KafkaBootstrapServers = "localhost:9092"
  val ClouderaZookeeperQuorum =  "172.17.0.2"

  val DeviceId1 = "cb3aef35-1f6f-40bf-875e-385601562b1f"
  val DeviceId2 = "72a7b303-645e-476f-8d5f-877991bf1cf8"
  val DeviceId3 = "ea225ada-9cdd-4edb-86a1-5b0d33e8950a"
}
