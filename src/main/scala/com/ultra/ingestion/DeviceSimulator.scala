package com.ultra.ingestion

import java.util.Date

import scala.util.Random

class DeviceSimulator(val deviceId: String, val latitude: Int, val longitude: Int) {

  def run(report: String => Unit): Unit = {

    val rnd = Random

    while (true) {
      val msg = newMessage(15 + rnd.nextInt(10))
      report(msg)
      println(s"Message sent: $msg")
      Thread.sleep(1000)
    }

  }

  private def newMessage(temperature: Int): String = {
    s"""
       |{
       |  "data": {
       |    "deviceId": "$deviceId",
       |    "temperature": $temperature,
       |   "location": {
       |     "latitude": $latitude,
       |     "longitude": $longitude },
       |    "time": ${new Date().getTime}
       |  }
       |}
     """.stripMargin
  }

}
