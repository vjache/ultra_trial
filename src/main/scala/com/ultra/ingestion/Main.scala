package com.ultra.ingestion

import cakesolutions.kafka.KafkaProducer
import cakesolutions.kafka.KafkaProducer.Conf
import com.ultra.Config
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer

object Main {

  val topic = "thermal_data"

  val producer = KafkaProducer(
    Conf(new StringSerializer(), new StringSerializer(), bootstrapServers = Config.KafkaBootstrapServers)
  )

  def main(args: Array[String]): Unit = {
    val devices = Seq(
      new DeviceSimulator(Config.DeviceId1, 33, 25),
      new DeviceSimulator(Config.DeviceId2, 60, 83),
      new DeviceSimulator(Config.DeviceId3, 10, 5))

    for (dev <- devices) {
      spawn(() => dev.run(reportToKafka))
    }
  }

  def spawn(task: () => Unit) {
    new Thread(
      new Runnable {
        override def run(): Unit = task()
      }
    ).start()
  }

  def reportToKafka(msg: String) {
    producer.send(new ProducerRecord(topic, msg))
  }
}
