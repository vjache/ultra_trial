package com.ultra.transformation.hbase

import java.text.SimpleDateFormat
import java.util.{Date, SimpleTimeZone}

import com.ultra.Config
import org.apache.hadoop.hbase.client.{Connection, ConnectionFactory, Put, Table}
import org.apache.hadoop.hbase.util.Bytes._
import org.apache.hadoop.hbase.{HBaseConfiguration, HColumnDescriptor, HTableDescriptor, TableName}
import org.json4s._
import org.json4s.jackson.JsonMethods._

object ThermalDataTable {

  case class Row(deviceId: String, temperature: Long, latitude: Long, longitude: Long, time: Long)

  val tableName: TableName = TableName.valueOf("thermal_data")

  private val mainColumnFamily = "main"
  private val mainColumnFamilyBytes = toBytes(mainColumnFamily)

  private val auxColumnFamily = "aux"
  private val auxColumnFamilyBytes = toBytes(auxColumnFamily)

  private val timeCol = "time"
  private val deviceIdCol = "deviceId"
  private val temperatureCol = "temperature"
  private val latitudeCol = "latitude"
  private val longitudeCol = "longitude"
  private val rawCol = "raw"

  private val timeColBytes = toBytes(timeCol)
  private val deviceIdColBytes = toBytes(deviceIdCol)
  private val temperatureColBytes = toBytes(temperatureCol)
  private val latitudeColBytes = toBytes(latitudeCol)
  private val longitudeColBytes = toBytes(longitudeCol)
  private val rawColBytes = toBytes(rawCol)

  private val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
  format.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"))

  // Instantiating configuration class// Instantiating configuration class
  private val hConf = HBaseConfiguration.create
  hConf.set("hbase.zookeeper.quorum", Config.ClouderaZookeeperQuorum)

  private val conn: Connection = ConnectionFactory.createConnection(hConf)

  val table: Table = { // Instantiating configuration class

    // Admin tool
    val admin = conn.getAdmin

    // Instantiating HbaseAdmin class
    if (!admin.tableExists(tableName)) {
      // Instantiating table descriptor class
      val tableDescriptor = new HTableDescriptor(tableName)

      // Adding column families to table descriptor
      tableDescriptor.addFamily(new HColumnDescriptor(mainColumnFamily))
      tableDescriptor.addFamily(new HColumnDescriptor(auxColumnFamily))

      // Execute the table through admin
      admin.createTable(tableDescriptor)
      System.out.println(" Table created ")
    } else {
      System.out.println(" Table exist ")
    }

    conn.getTable(tableName)
  }

  def jsonStrToMap(jsonStr: String): Map[String, Any] = {
    implicit val formats: DefaultFormats.type = org.json4s.DefaultFormats
    parse(jsonStr).extract[Map[String, Any]]
  }

  def insert(jsonStr: String): Either[String, Row] = {

    val tuple = for {
      data <- jsonStrToMap(jsonStr).get("data").map(_.asInstanceOf[Map[String, Any]])
      deviceId <- data.get(deviceIdCol).map(_.asInstanceOf[String])
      temperature <- data.get(temperatureCol).map(toLong)
      time <- data.get(timeCol).map(toLong)
      location <- data.get("location").map(_.asInstanceOf[Map[String, Any]])
      latitude <- location.get(latitudeCol).map(toLong)
      longitude <- location.get(longitudeCol).map(toLong)
    } yield {
      val row = Row(
        deviceId = deviceId,
        temperature = temperature,
        latitude = latitude,
        longitude = longitude,
        time = time)
      insert(jsonStr, row)
      row
    }

    tuple.toRight(s"Unexpected JSON representation:$jsonStr")
  }

  private def toLong(v: Any): Long = v.asInstanceOf[BigInt].longValue()

  def insert(jsonStr: String, dp: Row): Unit = {
    val iso8601 = format.format(new Date(dp.time))

    // Seems for one measurement it is enough to have as a key DevId+Timestamp.
    val row = new Put(toBytes(s"${dp.deviceId}:$iso8601"))

    row.addColumn(mainColumnFamilyBytes, deviceIdColBytes, toBytes(dp.deviceId))
    row.addColumn(mainColumnFamilyBytes, timeColBytes, toBytes(iso8601))
    row.addColumn(mainColumnFamilyBytes, temperatureColBytes, toBytes(dp.temperature))
    row.addColumn(mainColumnFamilyBytes, latitudeColBytes, toBytes(dp.latitude))
    row.addColumn(mainColumnFamilyBytes, longitudeColBytes, toBytes(dp.longitude))
    // OPTIMIZATION: Raw data better to store in a separate column family
    row.addColumn(auxColumnFamilyBytes, rawColBytes, toBytes(jsonStr))

    table.put(row)
  }
}
