package com.ultra.transformation

import java.io.File

import com.ultra.Config
import com.ultra.transformation.hbase.ThermalDataTable
import kafka.serializer.StringDecoder
import org.apache.spark.SparkConf
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}

object Main {
  val topic = "thermal_data"

  private val checkpointDir = {
    val cpDir = "./checkpoint"
    // TODO: This may have sense only for dev stage for clean run. Think how to separate production & dev behaviours.
    // Remove local checkpoint dir if exists. Local checkpoint dir used only for dev.
    val cpDirF: File = new File(cpDir)
    if (cpDirF.exists()) deleteDir(cpDirF)
    //
    cpDir
  }

  val ssc = StreamingContext.getOrCreate(checkpointDir, () => {
    // Make some bla-bla-bla to make Spark happy for local start
    val appName = "ultra"
    val master = "local[*]"
    val conf = new SparkConf().setAppName(appName).setMaster(master)
    val ssc = new StreamingContext(conf, Seconds(5))
    ssc.checkpoint(checkpointDir)
    ssc
  })

  def deleteDir(file: File): Unit = {
    if (file.isDirectory)
      file.listFiles.foreach(deleteDir)
    if (file.exists && !file.delete)
      throw new Exception(s"Unable to delete ${file.getAbsolutePath}")
  }

  def main(args: Array[String]): Unit = {
    val kafkaConf = Map(
      "client.id" -> "ultra",
      "metadata.broker.list" -> Config.KafkaBootstrapServers,
      "partitioner.class" -> "kafka.producer.DefaultPartitioner",
      "serializer.class" -> "kafka.serializer.DefaultEncoder"
    )
    val stream = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](ssc, kafkaConf, Set(topic))
    stream.mapValues(x => {
      ThermalDataTable.insert(x)
    }).print()

    ssc.start()
    ssc.awaitTermination()
  }
}
