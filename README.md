# Run required services

Run Kafka:

```bash
$ cd src/main/resources
$ docker-compose up zookeeper kafka
```

Run Cloudera Quickstart Docker:
```bash
$ docker run --privileged=true \                           
--hostname=quickstart.cloudera \
-t -i ${IMAGE_HASH} \
/usr/bin/docker-quickstart

```

Where image hash can be obtained e.g.:

```bash
$  docker images | grep "cloudera/quickstart"
cloudera/quickstart         latest              4239cd2958c6        2 years ago         6.34GB
```

# Run Device Simulators

Run class `com.ultra.ingestion.Main`.

# Run Transformation (Save to HBase)

Run class `com.ultra.transformation.Main`.

# Make HBase table accessible from Impala

This is done by Hive Web Terminal via:
```sqlite-sql
CREATE EXTERNAL TABLE thermal_data(deviceId string,time string,temperature bigint, latitude bigint, longitude bigint, key string) 
STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler'
WITH SERDEPROPERTIES ("hbase.columns.mapping" = "main:deviceId,main:time,main:temperature#b,main:latitude#b,main:longitude#b,:key")
TBLPROPERTIES ("hbase.table.name" = "thermal_data", "hbase.mapred.output.outputtable" = "thermal_data");
```