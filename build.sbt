name := "ultra"

version := "0.1"

scalaVersion := "2.11.8"

val sscKafkaVersion = "1.6.2"

val sparkVersion = "2.0.0"

resolvers += Resolver.bintrayRepo("cakesolutions", "maven")
libraryDependencies += "net.cakesolutions" %% "scala-kafka-client" % "2.0.0"

libraryDependencies += "org.apache.spark" %% "spark-streaming-kafka" % "1.6.3"
libraryDependencies += "org.apache.spark" %% "spark-core" % "2.3.2"
libraryDependencies += "org.apache.spark" %% "spark-yarn" % "2.3.2"
libraryDependencies += "org.apache.spark" %% "spark-streaming" % "2.3.2"

libraryDependencies += "org.apache.hbase" % "hbase-client" % "1.3.1"
libraryDependencies += "org.apache.hbase" % "hbase-common" % "1.3.1"
libraryDependencies += "org.apache.hbase" % "hbase" % "1.3.1"
libraryDependencies += "org.apache.hadoop" % "hadoop-common" % "2.5.1"
libraryDependencies += "org.apache.hadoop" % "hadoop-client" % "2.5.1"
// https://mvnrepository.com/artifact/org.json4s/json4s-jackson
libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.6.1"





